import org.telegram.telegrambots.ApiContext;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.logging.BotLogger;

import java.io.File;

public class Main  {

    public static void main(String[] args){
        ApiContextInitializer.init();


        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {

            botsApi.registerBot(new BotConfig());
            BotLogger.severe("START" , "Bot is started");
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }


}
