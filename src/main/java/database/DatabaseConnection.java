package database;

import org.telegram.telegrambots.logging.BotLogger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseConnection {

    Connection connection;
    Statement statement;
    ResultSet set;
    private static final String LOGTAG = "CONNECTIONDB";
    ArrayList<String> provinces = new ArrayList<String>();
    private HashMap<String , Integer> provincesMap = new HashMap<String, Integer>();
    private HashMap<Integer , String> citiesMap = new HashMap<Integer, String>();
    private Connection currentConnection;
    public DatabaseConnection(){
        this.currentConnection = openConnexion();
    }

    private Connection openConnexion(){
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DatabaseConstants.JDBC_URL ,
                    DatabaseConstants.DB_USERNAME ,
                    DatabaseConstants.DB_PASSWORD);

        } catch (SQLException e) {
            BotLogger.error(LOGTAG , e);
        }

        return connection;
    }

    private void closeConnection(){
        try {
            this.currentConnection.close();
        } catch (SQLException e) {
            BotLogger.error(LOGTAG , e);
        }
    }

    public ResultSet runSqlQuery(String query) throws SQLException {
        final Statement statement;
        statement = this.currentConnection.createStatement();
        return statement.executeQuery(query);
    }

    public Boolean executeQuery(String query) throws SQLException {
        final Statement statement = this.currentConnection.createStatement();
        return statement.execute(query);
    }

    public PreparedStatement getPreparedStatement(String query) throws SQLException {
        return this.currentConnection.prepareStatement(query);
    }

    public PreparedStatement getPreparedStatement(String query, int flags) throws SQLException {
        return this.currentConnection.prepareStatement(query, flags);
    }

    private void connectToDb(int key){

        try {
            connection = DriverManager.getConnection(DatabaseConstants.JDBC_URL , DatabaseConstants.DB_USERNAME
            , DatabaseConstants.DB_PASSWORD);

            statement = connection.createStatement();

            switch (key){
                case DatabaseConstants.PROVINCE_KEY:
                    set = statement.executeQuery(DatabaseConstants.SELECT_PROVINCES);

                    while (set.next()){
                       // System.out.println(set.getString(2));
                        provinces.add(set.getString(2));

                        provincesMap.put(set.getString(2) , set.getInt(1));
                    }

                    break;

                case DatabaseConstants.CITIES_KEY:

                    break;

                    default:
                        System.out.println("no such a key");
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try {
                if (statement!=null){
                    statement.close();
                }
                if (set!=null){
                    set.close();
                }
                if (connection!=null){
                    connection.close();
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    public ArrayList<String> getProvinces(){
        connectToDb(DatabaseConstants.PROVINCE_KEY);
        return provinces;
    }

    public int getProvinceMap(String p){
        connectToDb(DatabaseConstants.PROVINCE_KEY);
        return provincesMap.get(p);
    }


}
