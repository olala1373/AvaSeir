package database;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import javax.swing.text.html.HTMLDocument;
import java.io.PipedReader;
import java.sql.*;

public class DatabaseManager {
    private final static String LOGTAG = "DATABASEMANGER";
    private final static String EMPTY = "empty";
    private final static int DEFAULT = 0;
    private final static String ADMIN_QUERY = "SELECT us.phone_number , trip.passengers_count , itt.start_time , itt.end_time , itt.origin_zone , itt.dest_zone , itt.days , us.first_name , us.last_name\n" +
            "FROM users us INNER JOIN trips trip ON us.telegram_id = trip.user_id INNER JOIN in_town_table itt ON itt.trip_id = trip.id\n" +
            "WHERE trip.id = ?";

    private final static String ADMIN_OUT_QUERY = "SELECT us.phone_number , tr.passengers_count , ott.origin_city , ott.dest_province , ott.dest_city ,\n" +
            "  ott.start_month , ott.start_day , ott.end_month , ott.end_day , us.first_name , us.last_name FROM users us\n" +
            "INNER JOIN trips tr ON us.telegram_id = tr.user_id INNER JOIN out_town_table ott on tr.id = ott.trip_id\n" +
            "WHERE tr.id = ?;";



    private static volatile DatabaseConnection connection;
    private static volatile DatabaseManager instance ;




    private DatabaseManager(){
        connection = new DatabaseConnection();

    }

    public static DatabaseManager getInstance(){
        final DatabaseManager currentInstance;
        if (instance == null) {
            synchronized (DatabaseManager.class) {
                if (instance == null) {
                    instance = new DatabaseManager();
                }
                currentInstance = instance;
            }
        } else {
            currentInstance = instance;
        }
        return currentInstance;
    }

    public int setUserState(int state , int user_id){
        int flag = 0;
        PreparedStatement statement ;
        try{
                statement = connection.getPreparedStatement("insert into states (user_id , state) values(? , ?) ON CONFLICT (user_id) DO UPDATE SET state = ?");
                statement.setInt(1 , user_id);
                statement.setInt(2 , state);
                statement.setInt(3 , state);
                flag = statement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }


        return flag;
    }

    public int getUserState(int user_id){
        int state = 0;
        PreparedStatement statement;
        try {
            statement = connection.getPreparedStatement("select state from states where user_id = ? ");
            statement.setInt(1 , user_id);
            ResultSet set = statement.executeQuery();
            if (set.next()){
                state = set.getInt(1);
            }

        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return state;
    }

    public boolean isUserRegistered(int user_id){
        boolean flag = false;
        try {
            final PreparedStatement statement = connection.getPreparedStatement("select * from users where telegram_id = ?");
            statement.setInt(1 , user_id);
            ResultSet set = statement.executeQuery();
            String ph = "";
            String test1 = "empty";
            String test2 = "empty";
            System.out.println(test1.equals(EMPTY));

            if (set.next()){
                ph = set.getString(5);
                if (set.getRow() == 0 ){
                    flag = false;
                }
                else if (ph.trim().equals(EMPTY)){
                    System.out.println("ROW OF TABLE second" + set.getRow() + set.getString(5));
                    flag = false;
                }
                else {
                    System.out.println("ROW OF TABLE third" + set.getRow() + set.getString(5));
                    flag = true;
                }
            }



        } catch (SQLException e) {
            e.printStackTrace();
        }

        return flag;
    }


    public int addUser(int user_id ){
        PreparedStatement statement ;
        String help = "";
        int flag = 0;

        try {
            statement = connection.getPreparedStatement("insert into users (telegram_id , first_name , last_name , phone_number) values(? , ? , ? , ?) ON CONFLICT (telegram_id) DO UPDATE SET telegram_id = ?");
            statement.setInt(1 , user_id);
            statement.setString(2,EMPTY);
            statement.setString(3 , EMPTY);
            statement.setString(4 , EMPTY);
            statement.setInt(5 , user_id);
            flag = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return flag;
    }

    public int addFirstName(int user_id , String firstName){
        PreparedStatement statement ;
        int flag = 0 ;

        try {
            statement = connection.getPreparedStatement("update users set first_name = ? where telegram_id = ?");
            statement.setString(1 , firstName);
            statement.setInt(2,user_id);
            flag = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public int addLastName(int user_id , String lastName){
        PreparedStatement statement ;
        int flag = 0 ;

        try {
            statement = connection.getPreparedStatement("update users set last_name = ? where telegram_id = ?");
            statement.setString(1 , lastName);
            statement.setInt(2,user_id);
            flag = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;

    }

    public int addPhoneNumber(int user_id , String phoneNumber){
        PreparedStatement statement ;
        int flag = 0 ;

        try {
            statement = connection.getPreparedStatement("update users set phone_number = ? where telegram_id = ?");
            statement.setString(1 , phoneNumber);
            statement.setInt(2 , user_id);
            flag = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public int addInTownTrip(int user_id ){
        PreparedStatement statement ;
        PreparedStatement insertInTown;
        PreparedStatement temp_passenger;
        ResultSet set;
        int id  = 0;
        int flag = 0;

        try {
            statement = connection.getPreparedStatement("insert into trips (user_id , passengers_count , is_in_town) values(? , ? , ? )" , Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1 , user_id);
            statement.setInt(2,0);
            statement.setBoolean(3 , false);

            flag = statement.executeUpdate();

            set = statement.getGeneratedKeys();
            if (set.next()){
                id = set.getInt(1);
                System.out.println(id);
            }


            if (id != 0 ){
                insertInTown = connection.getPreparedStatement("insert into  in_town_table (trip_id , start_date_day" +
                        " , start_date_month  , start_time" +
                        " , end_time , origin_zone , dest_zone) values (? , 0 , 0 , 0 , 0 , 0 , 0)");
                insertInTown.setInt(1 , id);
                insertInTown.executeUpdate();
            }

            temp_passenger = connection.getPreparedStatement("insert into temp_passenger (user_id , trip_id) values (? , ?) on Conflict (user_id) do update set trip_id = ? ");
            temp_passenger.setInt(1 , user_id);
            temp_passenger.setInt(2 , id);
            temp_passenger.setInt(3 , id);

            temp_passenger.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public int addOutTownTrip(int user_id){
        PreparedStatement trip;
        PreparedStatement insert_out_town;
        PreparedStatement temp_passenger;
        ResultSet set;
        int id = 0;

        try {
            trip = connection.getPreparedStatement("insert into trips (user_id , passengers_count , is_in_town) values ( ? , 0 , ?)" , Statement.RETURN_GENERATED_KEYS);
            trip.setInt(1 , user_id);
            trip.setBoolean(2 , false);
            trip.executeUpdate();

            set = trip.getGeneratedKeys();

            if (set.next()){
                id = set.getInt(1);
            }

            if (id != 0){
                insert_out_town = connection.getPreparedStatement("insert into out_town_table (trip_id , origin_province " +
                        ", dest_province , origin_city , dest_city , start_day , start_month , end_day , end_month) values (" +
                        "? , ? , ? , ? , ? , 0 , 0 , 0 , 0)");
                insert_out_town.setInt(1 , id);
                insert_out_town.setString(2 , "فارس");
                insert_out_town.setString(3,EMPTY);
                insert_out_town.setString(4,EMPTY);
                insert_out_town.setString(5,EMPTY);
                insert_out_town.executeUpdate();

            }


            temp_passenger = connection.getPreparedStatement("insert into temp_passenger (user_id , trip_id) values (? , ?) on Conflict (user_id) do update set trip_id = ? ");
            temp_passenger.setInt(1 , user_id);
            temp_passenger.setInt(2 , id);
            temp_passenger.setInt(3 , id);

            temp_passenger.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public int addPassengerCount(int user_id , int passenger_count , int id){
        PreparedStatement statement;
        int result = 0 ;

        try {
            statement = connection.getPreparedStatement("update trips set passengers_count = ? where user_id = ? and id = ?");
            statement.setInt(1 , passenger_count);
            statement.setInt(2 , user_id);
            statement.setInt(3 , id);
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int addIsInTown(int user_id , boolean isInTown){
        PreparedStatement statement;
        int result = 0 ;

        try {
            statement = connection.getPreparedStatement("update trips set is_in_town = ? where user_id = ?");
            statement.setBoolean( 1 , isInTown);
            statement.setInt(2 , user_id);
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;

    }


    public int getTripId(int user_id){
        int id = 0;

        PreparedStatement statement ;

        try {
            statement = connection.getPreparedStatement("select trip_id from temp_passenger where user_id = ?");
            statement.setInt(1 , user_id);
            ResultSet set  = statement.executeQuery();

            if (set.next())
                id = set.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public int addOriginZone(int trip_id , int originZone){

        PreparedStatement statement;
        int res = 0 ;

        try {
            statement = connection.getPreparedStatement("update in_town_table set origin_zone = ? where trip_id = ?");
            statement.setInt(1 , originZone);
            statement.setInt(2 ,trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;

    }

    public int addDestZone(int trip_id , int destZone){

        PreparedStatement statement;
        int res = 0 ;

        try {
            statement = connection.getPreparedStatement("update in_town_table set dest_zone = ? where trip_id = ?");
            statement.setInt(1 , destZone);
            statement.setInt(2 ,trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;

    }


    public ResultSet getMonths(){
        PreparedStatement statement;
        ResultSet set = null;

        try {
            statement = connection.getPreparedStatement("select * from months");
            set = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return set;
    }

    public int dayOfMonth(String monthName){

        PreparedStatement statement;
        ResultSet set = null;
        int day = 0 ;
        try {
            statement = connection.getPreparedStatement("select month_days from months where month_name = ?");
            statement.setString(1 , monthName);

            set = statement.executeQuery();

            if (set.next()){
                day = set.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return day;
    }

    public int monthId(String monthName){
        PreparedStatement statement;
        ResultSet set = null;
        int id = 0 ;
        try {
            statement = connection.getPreparedStatement("select id from months where month_name = ?");
            statement.setString(1 , monthName);
            set = statement.executeQuery();
            if (set.next()){
                id = set.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }


    public int addStartDateMonthIn(int trip_id , int startDate){
        PreparedStatement statement;
        int res = 0 ;

        try {
            statement = connection.getPreparedStatement("update in_town_table set start_date_month = ? where trip_id = ?");
            statement.setInt(1 , startDate);
            statement.setInt(2 ,trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;

    }

    public int addStartDateDayIn(int trip_id , int startDate){
        PreparedStatement statement;
        int res = 0 ;

        try {
            statement = connection.getPreparedStatement("update in_town_table set start_date_day = ? where trip_id = ?");
            statement.setInt(1 , startDate);
            statement.setInt(2 ,trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public int addEndTimeIn(int trip_id , int end_time){
        PreparedStatement statement;
        int res = 0 ;

        try {
            statement = connection.getPreparedStatement("update in_town_table set end_time = ? where trip_id = ?");
            statement.setInt(1 , end_time);
            statement.setInt(2 ,trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public int addStartTimeIn(int trip_id , int start_time){
        PreparedStatement statement;
        int res = 0 ;

        try {
            statement = connection.getPreparedStatement("update in_town_table set start_time = ? where trip_id = ?");
            statement.setInt(1 , start_time);
            statement.setInt(2 ,trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }


    public int addDays(int trip_id , int days){
        PreparedStatement statement;
        int res = 0 ;
        try{
            statement = connection.getPreparedStatement("update in_town_table set days = ? where trip_id = ?");
            statement.setInt(1 , days);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return res;
    }

    public ResultSet getFarsCities(){
        PreparedStatement statement;
        ResultSet set = null;

        try {
            statement = connection.getPreparedStatement("select name from cities where \"provinceId\" = 17 ");
            set = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return set;
    }

    public ResultSet getProvinces(){
        PreparedStatement statement;
        ResultSet set= null;

        try {
            statement = connection.getPreparedStatement("select name from provinces");
            set = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return set;
    }

    public int addOriginCity(int trip_id , String city){
        PreparedStatement statement;

        int result = 0;
        try {
            statement = connection.getPreparedStatement("update out_town_table set origin_city = ? where trip_id = ?");
            statement.setString(1,city);
            statement.setInt(2,trip_id);

           result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int addDestProvince(int trip_id , String dest){
        PreparedStatement statement;
        int res = 0 ;

        try{
            statement = connection.getPreparedStatement("update out_town_table set dest_province = ? where trip_id = ?");
            statement.setString(1 , dest);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public int addDestCity(int trip_id , String dest){
        PreparedStatement statement;
        int res = 0 ;

        try{
            statement = connection.getPreparedStatement("update out_town_table set dest_city = ? where trip_id = ?");
            statement.setString(1 , dest);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public int addStartMonth(int trip_id , int month){
        PreparedStatement statement;
        int res = 0 ;

        try{
            statement = connection.getPreparedStatement("update out_town_table set start_month = ? where trip_id = ?");
            statement.setInt(1 , month);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public int addStartDay(int trip_id , int day){
        PreparedStatement statement;
        int res = 0 ;

        try{
            statement = connection.getPreparedStatement("update out_town_table set start_day = ? where trip_id = ?");
            statement.setInt(1 , day);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }


    public int addEndMonth(int trip_id , int day){
        PreparedStatement statement;
        int res = 0 ;

        try{
            statement = connection.getPreparedStatement("update out_town_table set end_month = ? where trip_id = ?");
            statement.setInt(1 , day);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public int addEndDay(int trip_id , int day){
        PreparedStatement statement;
        int res = 0 ;

        try{
            statement = connection.getPreparedStatement("update out_town_table set end_day = ? where trip_id = ?");
            statement.setInt(1 , day);
            statement.setInt(2 , trip_id);

            res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public ResultSet selectAdmin(int trip_id){
        PreparedStatement statement;
        ResultSet set = null;
        try {
            statement = connection.getPreparedStatement(ADMIN_QUERY);
            statement.setInt(1 , trip_id);
            set = statement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return set;

    }

    public ResultSet selectAdminOut(int trip_id){
        PreparedStatement statement;
        ResultSet set = null;

        try{
            statement = connection.getPreparedStatement(ADMIN_OUT_QUERY);
            statement.setInt(1 , trip_id);

            set= statement.executeQuery();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return set;
    }


}
