
import database.DatabaseManager;
import messageHandling.MessageHandling;
import messageHandling.Messages;

import org.apache.tika.language.LanguageIdentifier;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.*;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BotConfig extends TelegramLongPollingBot {

    private final static String BOTTOKEN = "409507508:AAHljr22lKy2S54o7W21mrBFbRYRB5p3P9s";
    private final static String BOTUSERNAME = "ShzVanBot";
    private final static String TAG = "UPDATE";
    private int currentState = 0;
    private SendMessage sendMessage = new SendMessage();
    private int count = 0 ;

    private ArrayList<KeyboardRow> key = new ArrayList<KeyboardRow>();
    private ReplyKeyboardMarkup reply = new ReplyKeyboardMarkup();
    private long chat_Id;
    private DatabaseManager databaseManager;

    private String firstName = "";
    private String lastName = "";
    private String phoneNumber = "";

    public void onUpdateReceived(Update update) {
        BotLogger.severe(TAG, "update received");

        Message m = update.getMessage();
        databaseManager = DatabaseManager.getInstance();

        if (update.hasMessage() && update.getMessage().hasText()) {
            String message_text = update.getMessage().getText();
            chat_Id = update.getMessage().getChatId();
            handelIncomingMessage(m);
        }
    }

    public String getBotUsername() {
        return BOTUSERNAME;
    }

    public String getBotToken() {
        return BOTTOKEN;
    }

    private void sendMyMessages(SendMessage... sendMessage) {
        SendMessage sm = null;
        if (sendMessage.length>1){
            sm = sendMessage[1];
        }

        try {
            execute(sendMessage[0]);
            if (sm != null) {
                execute(sm);
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    public void handelIncomingMessage(Message message){


        int state;
        int user_id = message.getFrom().getId();
        long chat_id = message.getChatId();
        Location location;

        String message_text = "";

        if (message.hasText()){
            message_text = message.getText();
        }

        if (message.isUserMessage()){
            if (message.isCommand()){
                if (message.getText().equals(Commands.START)){
                    if (!databaseManager.isUserRegistered(user_id)){
                        sendMyMessages(MessageHandling.start(chat_id) ,
                                MessageHandling.notRegisteredUser(chat_id));
                        databaseManager.setUserState(Messages.NO_REG_STATE , user_id);
                        databaseManager.addUser(user_id);
                    }else {
                        databaseManager.setUserState(Messages.START_STATE , user_id);
                       sendMyMessages(MessageHandling.alreadyRegistered(chat_id));
                    }
                }
            }
            else {

                 state = databaseManager.getUserState(user_id);

                BotLogger.info(TAG , "user state is : " + state);


                 switch (state){
                     case Messages.NO_REG_STATE:

                         if (message_text.equals(Messages.ACC_REGISTERATION)){
                             databaseManager.setUserState(Messages.ENTER_INFORMATION , user_id);
                             sendMyMessages(MessageHandling.enterName(chat_id));
                             break;
                         }
                         else if (message_text.equals(Messages.REF_REGISTERATION)){

                         }
                         break;
                     case Messages.ENTER_INFORMATION:

                         if (languageDetector(message_text)){
                             firstName = message_text;
                             databaseManager.addFirstName(user_id , firstName);
                             databaseManager.setUserState(Messages.ENTER_NAME , user_id);
                             sendMyMessages(MessageHandling.enterLastName(chat_id));
                         }
                         else {
                             sendMyMessages(MessageHandling.enterInfoPersian(chat_id));
                         }

                         break;
                     case Messages.ENTER_NAME:
                         if (languageDetector(message_text)){
                             lastName = message_text;
                             databaseManager.addLastName(user_id , lastName);
                             databaseManager.setUserState(Messages.ENTER_LASTNAME , user_id);
                             sendMyMessages(MessageHandling.enterPhoneNumber(chat_id));
                         }
                         else {
                             sendMyMessages(MessageHandling.enterInfoPersian(chat_id));
                         }
                         break;
                     case Messages.ENTER_LASTNAME:
                         if (phoneNumberDetector(message_text)){
                             phoneNumber = message_text;
                            int s  = databaseManager.addPhoneNumber(user_id , phoneNumber);
                             databaseManager.setUserState(Messages.START_STATE , user_id);

                             if (s != 0){
                                 sendMyMessages(MessageHandling.succesfullRegisteration(chat_id));
                             }
                         }
                         else
                             sendMyMessages(MessageHandling.enterCorrectPhoneNumber(chat_id));
                         break;
                     case Messages.START_STATE:
                         if (message_text.equals(Messages.OK)){
                             sendMyMessages(MessageHandling.startTripRequest(chat_id));
                         }


                         if (message_text.equals(Messages.INTOWN)){
                             databaseManager.addInTownTrip(user_id);
                             sendMyMessages(MessageHandling.requestForInTown(chat_id));
                             databaseManager.setUserState(Messages.IN_TOWN_STATE , user_id);
                             databaseManager.addIsInTown(user_id , true);

                         }
                         if (message_text.equals(Messages.OUTOFTOWN)){
                             databaseManager.addOutTownTrip(user_id);
                             sendMyMessages(MessageHandling.requestForOutTown(chat_id));
                             databaseManager.setUserState(Messages.OUTOF_TOWN_STATE , user_id);
                             databaseManager.addIsInTown(user_id , false);
                         }
                         break;

                 }

                 if (state >= Messages.IN_TOWN_STATE && state < Messages.OUTOF_TOWN_STATE){
                     inTownRequests(chat_id , state , message , user_id);
                 }

                 if (state >= Messages.OUTOF_TOWN_STATE && state < Messages.END_REQUEST){
                     outOfTownRequests(chat_id , state , message , user_id);
                 }
            }
        }
        else{
            System.out.println("not users");
        }

    }

    public boolean languageDetector(String text){

        LanguageIdentifier identifier = new LanguageIdentifier(text);
        String lang = identifier.getLanguage();
        System.out.println(lang);
        if (lang.equals("fa") || lang.equals("lt")){
            return true;
        }
        else {
            return false;
        }

    }

    public boolean phoneNumberDetector(String phonenunber){

        if (phonenunber.length() != 10){
            return false;
        }
        else if (phonenunber.startsWith("0")){
            return false;
        }
        else if (phonenunber.startsWith("9")){
            return true;
        }
        else {
            return false;
        }
    }


    public void inTownRequests(long chat_Id , int state , Message message , int user_id){

        String message_text = "";
        if (message.hasText()){
            message_text = message.getText();
        }

        switch (state){
            case Messages.IN_TOWN_STATE:

                if (message_text.equals(Messages.OK)){
                    databaseManager.setUserState(Messages.ENTER_PSSENGERS_INTOWN , user_id);
                    sendMyMessages(MessageHandling.enterPassengerCount(chat_Id));
                }
                else if (message_text.equals(Messages.REF_REGISTERATION)){
                    databaseManager.setUserState(Messages.START_STATE , user_id);

                }
                break;
            case Messages.ENTER_PSSENGERS_INTOWN:

                if (isNumber(message_text)){
                    int passenger = Integer.parseInt(message_text);
                    int id = databaseManager.getTripId(user_id);
                    databaseManager.addPassengerCount(user_id , passenger , id);
                    int s = databaseManager.setUserState(Messages.ENTER_ORIGIN_LOCATION_ZONE , user_id);
                    System.out.println(s);
                    try {
                        sendPhoto(MessageHandling.enterStartRegion(chat_Id));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setText(Messages.ENTER_CORRECT_PASSENGER_COUNT);
                    sendMessage.setChatId(chat_Id);
                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Messages.ENTER_ORIGIN_LOCATION_ZONE:
                if (isNumber(message_text)){
                    int origin = Integer.parseInt(message_text);
                    int id = databaseManager.getTripId(user_id);

                    databaseManager.addOriginZone(id , origin);
                    databaseManager.setUserState(Messages.ENTER_DEST_LOCATION_ZONE , user_id);

                    try {
                        sendPhoto(MessageHandling.enterEndRegion(chat_Id));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setText(Messages.ENTER_CORRECT_COMMAND);
                    sendMessage.setChatId(chat_Id);
                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Messages.ENTER_DEST_LOCATION_ZONE:
                if (isNumber(message_text)){
                    int dest = Integer.parseInt(message_text);
                    int id = databaseManager.getTripId(user_id);

                    databaseManager.addDestZone(id , dest);
                    databaseManager.setUserState(Messages.ENTER_START_MONTH , user_id);
                    sendMyMessages(MessageHandling.enterStartMonth(chat_Id));
                }
                else {
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setText(Messages.ENTER_CORRECT_COMMAND);
                    sendMessage.setChatId(chat_Id);
                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case Messages.ENTER_START_MONTH:
                String[] index = message_text.split("[)]");
                int trip_id = databaseManager.getTripId(user_id);
                int day = databaseManager.dayOfMonth(index[1].trim());
                int month  = databaseManager.monthId(index[1].trim());
                databaseManager.addStartDateMonthIn(trip_id , month);
                databaseManager.setUserState(Messages.ENTER_START_DAY , user_id);
                sendMyMessages(MessageHandling.enterDayOfMonth(chat_Id , day));
                break;

            case Messages.ENTER_START_DAY:
                if (isNumber(message_text)){
                    int numDay = Integer.parseInt(message_text);
                    trip_id = databaseManager.getTripId(user_id);
                    databaseManager.addStartDateDayIn(trip_id , numDay);
                    databaseManager.setUserState(Messages.ENTER_START_HOUR , user_id);
                    sendMyMessages(MessageHandling.enterHourStart(chat_Id));
                }
                else {
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setText(Messages.ENTER_CORRECT_COMMAND);
                    sendMessage.setChatId(chat_Id);
                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                break;


            case Messages.ENTER_START_HOUR:
                if (isNumber(message_text)){
                    trip_id = databaseManager.getTripId(user_id);
                    int time = Integer.parseInt(message_text);
                    databaseManager.addStartTimeIn(trip_id , time);
                    databaseManager.setUserState(Messages.ENTER_END_HOUR , user_id);
                    sendMyMessages(MessageHandling.enterHourEnd(chat_Id));
                }
                else {
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setText(Messages.ENTER_CORRECT_COMMAND);
                    sendMessage.setChatId(chat_Id);
                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Messages.ENTER_END_HOUR:
                if (isNumber(message_text)){
                    trip_id = databaseManager.getTripId(user_id);
                    int time = Integer.parseInt(message_text);
                    databaseManager.addEndTimeIn(trip_id , time);
                    databaseManager.setUserState(Messages.ENTER_DAYS , user_id);
                    sendMyMessages(MessageHandling.howManyDays(chat_Id));
                }
                else {
                   sendMyMessages(MessageHandling.enterCorrectCommand(chat_Id));
                }
                break;
            case Messages.ENTER_DAYS:
                if (isNumber(message_text)){
                    trip_id = databaseManager.getTripId(user_id);
                    int days = Integer.parseInt(message_text);
                    databaseManager.addDays(trip_id , days);
                    databaseManager.setUserState(Messages.END_REQUEST , user_id);

                    sendMyMessages(MessageHandling.endOfrequests(chat_Id));

                    ResultSet set = databaseManager.selectAdmin(trip_id);
                    extractInfoForAdmin(set , trip_id);
                }
                else {
                    sendMyMessages(MessageHandling.enterCorrectCommand(chat_Id));
                }
                break;
        }
    }

    public void outOfTownRequests(long chat_Id , int state , Message message , int user_id){
        String message_text = "";

        if (message.hasText()){
            message_text = message.getText();
        }

        switch (state){
            case Messages.OUTOF_TOWN_STATE:
                if (message_text.equals(Messages.OK)){
                    databaseManager.setUserState(Messages.PASSENGER_OUTTOWN_STATE , user_id);
                    sendMyMessages(MessageHandling.enterPassengerCount(chat_Id));
                }
                else if (message_text.equals(Messages.REF_REGISTERATION)){
                    databaseManager.setUserState(Messages.START_STATE , user_id);
                }
                break;
            case Messages.PASSENGER_OUTTOWN_STATE:
                if (isNumber(message_text)){
                    int pass = Integer.parseInt(message_text);
                    int id = databaseManager.getTripId(user_id);
                    databaseManager.addPassengerCount(user_id , pass , id);
                    databaseManager.setUserState(Messages.ORIGIN_CITY_STATE , user_id);
                    sendMyMessages(MessageHandling.enterOriginCity(chat_Id));
                }
                else {
                    sendMyMessages(MessageHandling.enterCorrectCommand(chat_Id));
                }
                break;

            case Messages.ORIGIN_CITY_STATE:
                int id = databaseManager.getTripId(user_id);
                databaseManager.addOriginCity(id , message_text);
                databaseManager.setUserState(Messages.DEST_PROVINCE_STATE , user_id);
                sendMyMessages(MessageHandling.enterDestProvince(chat_Id));
                break;
            case Messages.DEST_PROVINCE_STATE:
                id = databaseManager.getTripId(user_id);
                databaseManager.addDestProvince(id , message_text);
                databaseManager.setUserState(Messages.DEST_CITY_STATE , user_id);
                sendMyMessages(MessageHandling.enterDestCity(chat_Id));
                break;
            case Messages.DEST_CITY_STATE:
                id = databaseManager.getTripId(user_id);
                databaseManager.addDestCity(id , message_text);
                databaseManager.setUserState(Messages.START_MONTH_STATE , user_id);
                sendMyMessages(MessageHandling.enterStartMonth(chat_Id));
                break;

            case Messages.START_MONTH_STATE:
                String[] index = message_text.split("[)]");
                id = databaseManager.getTripId(user_id);
                int day = databaseManager.dayOfMonth(index[1].trim());
                int month  = databaseManager.monthId(index[1].trim());
                databaseManager.setUserState(Messages.START_DAY_STATE , user_id);
                databaseManager.addStartMonth(id , month);
                sendMyMessages(MessageHandling.enterDayOfMonth(chat_Id , day));
                break;

            case Messages.START_DAY_STATE:
                if (isNumber(message_text)){
                    id = databaseManager.getTripId(user_id);
                    day = Integer.parseInt(message_text);
                    databaseManager.addStartDay(id , day);
                    databaseManager.setUserState(Messages.HAS_RETURN_STATE , user_id);
                    sendMyMessages(MessageHandling.hasReturn(chat_Id));
                }
                else {
                    sendMyMessages(MessageHandling.enterCorrectCommand(chat_Id));
                }
                break;

            case Messages.HAS_RETURN_STATE:
                if (message_text.equals(Messages.YES)){
                    databaseManager.setUserState(Messages.END_MONTH_STATE , user_id);
                    sendMyMessages(MessageHandling.returnMonth(chat_Id));
                }
                else {
                    id = databaseManager.getTripId(user_id);
                    sendMyMessages(MessageHandling.endOfrequests(chat_Id));
                    ResultSet set = databaseManager.selectAdminOut(id);
                    extractInfoOutForAdmin(set , id);
                }
                break;

            case Messages.END_MONTH_STATE:
                index = message_text.split("[)]");
                id = databaseManager.getTripId(user_id);
                day = databaseManager.dayOfMonth(index[1].trim());
                month  = databaseManager.monthId(index[1].trim());
                databaseManager.addEndMonth(id , month);
                databaseManager.setUserState(Messages.END_DAY_STATE , user_id);
                sendMyMessages(MessageHandling.returnDay(chat_Id , day));
                break;

            case Messages.END_DAY_STATE:
                if (isNumber(message_text)){
                    day = Integer.parseInt(message_text);
                    id = databaseManager.getTripId(user_id);
                    databaseManager.addEndDay(id , day);
                    databaseManager.setUserState(Messages.END_REQUEST , user_id);
                    sendMyMessages(MessageHandling.endOfrequests(chat_Id));

                    ResultSet set = databaseManager.selectAdminOut(id);
                    extractInfoOutForAdmin(set , id);

                }
                else {
                    sendMyMessages(MessageHandling.enterCorrectCommand(chat_Id));
                }
                break;


        }
    }

    public boolean isNumber(String message){
        try {
            Integer.parseInt(message);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public void extractInfoForAdmin(ResultSet set , int id){
        try {
            if (set.next()){
              String num = set.getString(1);
              int pas = set.getInt(2);
              int st = set.getInt(3);
              int en = set.getInt(4);
              int orig = set.getInt(5);
              int des = set.getInt(6);
              int days = set.getInt(7);
              String name = set.getString(8);
              String last_name = set.getString(9);

                System.out.println(name + " " + last_name);

              sendMyMessages(MessageHandling.sendToAdmin(num , pas , st , en , orig , des , days , id , name , last_name));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void extractInfoOutForAdmin(ResultSet set , int id){
        try {
            if (set.next()){
              String num = set.getString(1);
              int pas = set.getInt(2);
              String orgc = set.getString(3);
              String desp = set.getString(4);
              String desc = set.getString(5);
              int smon = set.getInt(6);
              int sday = set.getInt(7);
              int emon = set.getInt(8);
              int eday = set.getInt(9);
              String name = set.getString(10);
              String last_name = set.getString(11);

              System.out.println(name + " " + last_name);

              sendMyMessages(MessageHandling.sendToAdminOut(num , pas , orgc , desp , desc , smon , sday , emon , eday , id , name , last_name));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}

