package messageHandling;

import database.DatabaseManager;
import org.apache.poi.ss.formula.functions.Na;
import org.omg.CORBA.DATA_CONVERSION;

import javax.xml.crypto.Data;
import java.io.File;

public class Messages {

    /*Reply messages_en_US*/
    public final static String START_MSG = "سلام به ربات شیراز ون خوش آمدید \nاین ربات به منظور تسهیل حمل و نقل بین شهری ایجاد شده است.";
    public final static String TRAVEL_REQ_MSG = "لطفا یکی از گزینه های زیر را انتخاب کنید: ";
    public final static String ENTER_PASSENGERS_COUNT_MSG = "لطفا تعداد نفرات خود را وارد کنید:";
    public final static String SELECT_HOME_PROVINCE = "لطفا استان مبدا خود را انتخاب کنید";
    public final static String END_OF_REQUESTS = "در اسرع وقت با شما تماس گرفته می شود. برای شروع دوباره می توانید /start را بزنید";
    public final static String ENTER_CORRECT_PASSENGER_COUNT = "تعداد مسافران را درست وارد کنید";


    /*REGISTRATION*/
    public final static String ALREADY_REGISTERED ="شما در سیستم قبلا ثبت شده اید";
    public final static String USER_NOT_REGISTER = "شما در سیستم ثبت نام نشده اید \n آیا می خواهید ثبت نام کنید؟";
    public final static String ENTER_LAST_NAME = "نام خانوادگی خود را به زبان فارسی وارد کنید:";
    public final static String ENTER_PHONE_NUMBER = "شماره تلفن خود بدون 0 اول وارد کنید را وارد کنید:(9361234567) لطفا کیبورد خود را انگلیسی کنید .";
    public final static String ENTER_FIRST_NAME = "نام خود را به زبان فارسی وارد کنید:";
    public final static String ENTER_IN_PERSIAN = "لطفا اطلاعات خود را به زبان فارسی وارد کنید!";
    public final static String SUCCESFULL_REGISTERATION = "اطلاعات شما با موفقیت ذخیره شد";


    /*In town messages*/
    public final static String ENTER_END_REGION = "مقصد شما در کدام محدوده قرار دارد؟";
    public final static String ENTER_START_REGION = "مبدا شما جزو کدام محدوده می باشد:؟";
    public final static String INTOWN_REQUEST_RIDE = "شما درخواست سرویس درون شهری داده اید لطفا به سوالات با دقت جواب دهید!";
    public final static String ENTER_CORRECT_COMMAND = "لطفا دستور درست را وارد کنید.";
    public final static String ENTER_MONTH_START = "لطفا ماهی که می خواهید سرویس را در اختیار داشته باشید انتخاب کنید:";
    public final static String ENTER_HOUR_START = "لطفا ساعت حرکت خود را مشخص کنید";
    public final static String ENTER_HOUR_END = "لطفا ساعت برگشت خود را مشخص کنید";
    public final static String DAYS_SERVICE = "این سرویس را برای چند روز نیاز دارید؟";

    /*Out town messages*/
    public final static String OUTTOWN_REQUEST_RIDE = "شما درخواست سرویس برون شهری داده اید لطفا به سوالات با دقت جواب دهید!";
    public final static String ENTER_ORIGIN_CITY = "لطفا شهر مبدا خود را انتخاب کنید:";
    public final static String ENTER_DEST_PROVINCE = "لطفا استان مقصد خود را انتخاب کنید:";
    public final static String ENTER_DEST_CITY = "لطفا شهر مقصد را به زبان فارسی بنویسید:";
    public final static String ENTER_END_MONTH = "لطفا ماه برگشت خود را انتخاب کنید:";
    public final static String ENTER_END_DAY = "لطفا روز برگشت خود را انتخاب کنید:";
    public final static String HAS_RETURN = "آیا قصد برگشت دارید؟";
    public final static String YES = "بله";
    public final static String NO = "خیر";



    /*In keyboard messages_en_US*/
    public final static String RETURN_MSG = "صفحه اصلی";
    public final static String INTOWN = "درون شهری";
    public final static String OUTOFTOWN = "برون شهری";
    public final static String ACC_REGISTERATION = "می خواهم ثبت نام کنم";
    public final static String REF_REGISTERATION = "فعلا نه";
    public final static String OK = "باشه";

    /*Key states*/
    public final static int START_STATE = 0;
    public final static int MAIN_MENU = 1;

    /*Registration states*/
    public final static int NO_REG_STATE = -1;
    public final static int ENTER_INFORMATION = 100;
    public final static int ENTER_NAME = 101;
    public final static int ENTER_LASTNAME = 102;
    public final static int ENTER_PHONENUMBER = 103;
    public final static int END_REG_STATE = 104;

    /*In town STATES*/
    public final static int IN_TOWN_STATE = 200 ;
    public final static int ENTER_PSSENGERS_INTOWN = 201;
    public final static int ENTER_ORIGIN_LOCATION_ZONE = 202;
    public final static int ENTER_DEST_LOCATION_ZONE = 203;
    public final static int ENTER_START_MONTH = 204;
    public final static int ENTER_START_DAY = 205;
    public final static int ENTER_START_HOUR = 206;
    public final static int ENTER_END_HOUR = 207;
    public final static int ENTER_DAYS = 208;



    /*Out of town STATES*/
    public final static int OUTOF_TOWN_STATE = 300 ;
    public final static int PASSENGER_OUTTOWN_STATE = 301 ;
    public final static int ORIGIN_CITY_STATE = 302 ;
    public final static int DEST_PROVINCE_STATE = 303 ;
    public final static int DEST_CITY_STATE = 304 ;
    public final static int START_MONTH_STATE = 305 ;
    public final static int START_DAY_STATE = 306 ;
    public final static int HAS_RETURN_STATE = 307;
    public final static int END_MONTH_STATE = 308 ;
    public final static int END_DAY_STATE = 309 ;








    public final static int END_REQUEST = 1000;




    public static String EnterDayOfMonth(int day){
        String s = "";
        s = String.format( "لطفا روز حرکت خود را وارد کنید: عدد شما باید بین 1 و %d باشد." , day);
        return s;
    }


    public static long ADMIN_ID = 287088886;


}
