package messageHandling;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import database.DatabaseConnection;
import database.DatabaseManager;
import messageHandling.Messages;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import javax.xml.crypto.Data;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ReplyKeyBoard {

    private static ArrayList<KeyboardRow> key = new ArrayList<KeyboardRow>();
    static InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
    static InlineKeyboardButton but = new InlineKeyboardButton();

    public static ArrayList<KeyboardRow> mainMenu(){
        key.clear();
        KeyboardRow firstrow = new KeyboardRow();
        firstrow.add(0 , Messages.INTOWN);
        firstrow.add(1 , Messages.OUTOFTOWN);
        key.add(firstrow);
        return key;
    }

    public static ArrayList<KeyboardRow> registeration(){
        KeyboardRow firstRow = new KeyboardRow();
        firstRow.add(Messages.REF_REGISTERATION);
        firstRow.add(Messages.ACC_REGISTERATION);
        key.clear();
        key.add(firstRow);

        return key;
    }

    public static ArrayList<KeyboardRow> endOfRegisteration(){
        KeyboardRow firstRow = new KeyboardRow();
        firstRow.add(Messages.OK);
        key.clear();
        key.add(firstRow);
        return key;
    }

    public static ArrayList<KeyboardRow> acceptInTownRequest(){
        KeyboardRow firstRow = new KeyboardRow();
        firstRow.add(Messages.OK);
        firstRow.add(Messages.REF_REGISTERATION);
        key.clear();
        key.add(firstRow);
        return key;
    }

    public static ArrayList<KeyboardRow> cityZone(){
        KeyboardRow firstRow = new KeyboardRow();
        KeyboardRow secondRow = new KeyboardRow();
        firstRow.add("1");
        firstRow.add("2");
        firstRow.add("3");
        firstRow.add("4");
        secondRow.add("5");
        secondRow.add("6");
        secondRow.add("7");
        secondRow.add("8");
        key.clear();
        key.add(firstRow);
        key.add(secondRow);
        return key;
    }

    public static ArrayList<KeyboardRow> alreadyRegistered(){
        KeyboardRow firstRow = new KeyboardRow();
        firstRow.add(Messages.OK);
        key.clear();
        key.add(firstRow);
        return key;
    }

    public static ArrayList<KeyboardRow> months(){
        DatabaseManager databaseManager = DatabaseManager.getInstance();
        ResultSet set = databaseManager.getMonths();
        KeyboardRow firstRow = new KeyboardRow();
        KeyboardRow secondRow = new KeyboardRow();
        KeyboardRow thirdRow = new KeyboardRow();
        KeyboardRow fourthRow = new KeyboardRow();
        try {
            int k = 0 ;
            int mNumber ;
            String monthName ;
            while (set.next()){
                if (k < 3){
                    mNumber = set.getInt(1);
                    monthName = set.getString(2);
                    firstRow.add("(" + mNumber + ")" + monthName);
                    k++;
                }
                else if (k < 6){
                    mNumber = set.getInt(1);
                    monthName = set.getString(2);
                    secondRow.add("(" + mNumber + ")" + monthName);
                    k++;
                }
                else if ( k < 9){
                    mNumber = set.getInt(1);
                    monthName = set.getString(2);
                    thirdRow.add("(" + mNumber + ")" + monthName);
                    k++;
                }
                else if (k < 12){
                    mNumber = set.getInt(1);
                    monthName = set.getString(2);
                    fourthRow.add("(" + mNumber + ")" + monthName);
                    k++;
                }
            }
            key.clear();
            key.add(firstRow);
            key.add(secondRow);
            key.add(thirdRow);
            key.add(fourthRow);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return key;
    }

    public static   ArrayList<KeyboardRow> hours(){
        KeyboardRow firstRow = new KeyboardRow();
        KeyboardRow secondRow = new KeyboardRow();
        KeyboardRow thirdRow = new KeyboardRow();

        for (int i = 0 ; i < 7 ; i++){
            firstRow.add("" + (i + 1));
        }
        for (int i = 7 ; i < 16 ; i++){
            secondRow.add("" + (i + 1));
        }
        for (int i = 16 ; i < 24 ; i++){
            thirdRow.add("" + (i + 1));
        }

        key.clear();
        key.add(firstRow);
        key.add(secondRow);
        key.add(thirdRow);

        return key;
    }


    public static ArrayList<KeyboardRow> originCities(){
        DatabaseManager databaseManager = DatabaseManager.getInstance();
        KeyboardRow[] keyboardRows = new KeyboardRow[6];
        ResultSet set = databaseManager.getFarsCities();
        int k = 0 ;
        int i = 0 ;
        int t = 0;
        try {
            for (int j = 0 ; j < 6 ; j++){
                keyboardRows[j] = new KeyboardRow();
            }
            while (set.next()){

                if (k < 24){
                    if (t<3){
                        keyboardRows[i].add(set.getString(1));
                        t++;
                    }
                    else {
                        keyboardRows[i].add(set.getString(1));
                        i++;
                        t = 0;
                    }

                    k++;
                }

            }

            String first = keyboardRows[0].get(0).getText();
            String second = keyboardRows[2].get(2).getText();
            keyboardRows[0].remove(0);
            keyboardRows[2].remove(2);
            keyboardRows[0].add(0 , second);
            keyboardRows[2].add(2,first);

        }
        catch (SQLException e){
            e.printStackTrace();
        }
        key.clear();
        key.add(keyboardRows[0]);
        key.add(keyboardRows[1]);
        key.add(keyboardRows[2]);
        key.add(keyboardRows[3]);
        key.add(keyboardRows[4]);
        key.add(keyboardRows[5]);

        return key;
    }

    public static ArrayList<KeyboardRow> provinces(){
        DatabaseManager databaseManager = DatabaseManager.getInstance();
        ResultSet set = databaseManager.getProvinces();
        KeyboardRow[] keyboardRows = new KeyboardRow[8];
        int k = 0 ;
        int j = 0;
        for (int i = 0 ; i < 8 ; i++){
            keyboardRows[i] = new KeyboardRow();
        }
        try {

            while (set.next()){
                if (k < 3){
                    keyboardRows[j].add(set.getString(1));
                    k++;
                }
                else {
                    keyboardRows[j].add(set.getString(1));
                    j++;
                    k = 0;
                }
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        key.clear();
        for (int i = 0 ; i < 8 ; i++){
            key.add(keyboardRows[i]);
        }
        return key;
    }

    public static ArrayList<KeyboardRow> hasReturn(){

        KeyboardRow row = new KeyboardRow();

        row.add(Messages.YES);
        row.add(Messages.NO);

        key.clear();
        key.add(row);

        return key;
    }



}
