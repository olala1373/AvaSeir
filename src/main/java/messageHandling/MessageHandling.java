package messageHandling;

import org.apache.poi.ss.formula.functions.Na;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import sun.misc.resources.Messages_es;

import javax.print.DocFlavor;
import java.io.File;

public class MessageHandling {
    private static ReplyKeyboardMarkup reply = new ReplyKeyboardMarkup();
    public static String ImgUrl = "https://drive.google.com/open?id=1230CnDqsD2dtWasODg-Aurx83ny1DZyu";

    public static SendMessage start(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.START_MSG);
        sendMessage.setChatId(id);
        return sendMessage;
    }

    public static SendMessage enterPassengerCount(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_PASSENGERS_COUNT_MSG);
        sendMessage.setChatId(id);
        return sendMessage;
    }

    public static SendMessage enterName(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_FIRST_NAME);
        sendMessage.setChatId(id);
        return sendMessage;
    }

    public static SendMessage enterLastName(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_LAST_NAME);
        sendMessage.setChatId(id);
        return sendMessage;
    }

    public static SendMessage enterPhoneNumber(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_PHONE_NUMBER);
        sendMessage.setChatId(id);
        return sendMessage;
    }

    public static SendMessage notRegisteredUser(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.USER_NOT_REGISTER);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.registeration());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }

    public static SendMessage enterInfoPersian(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_IN_PERSIAN);
        sendMessage.setChatId(id);
        sendMessage.enableMarkdown(true);
        return sendMessage;
    }

    public static SendMessage enterCorrectPhoneNumber(long id){
        SendMessage sendMessage = null ;
        sendMessage = new SendMessage();
        sendMessage.setText("لطفا شماره موبایل خود را درست وارد کنید!");
        sendMessage.setChatId(id);
        sendMessage.enableMarkdown(true);
        return sendMessage;
    }

    public static SendMessage succesfullRegisteration(long id){
        SendMessage sendMessage = null;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.SUCCESFULL_REGISTERATION);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.endOfRegisteration());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }

    public static SendMessage startTripRequest(long id){
        SendMessage sendMessage = null;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.TRAVEL_REQ_MSG);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.mainMenu());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }


    public static SendMessage requestForInTown(long id){
        SendMessage sendMessage = null;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.INTOWN_REQUEST_RIDE);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.acceptInTownRequest());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        reply.setSelective(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }

    public static SendMessage requestForOutTown(long id){
        SendMessage sendMessage = null;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.OUTTOWN_REQUEST_RIDE);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.acceptInTownRequest());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        reply.setSelective(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }

    public static SendMessage alreadyRegistered(long id ){
        SendMessage sendMessage = null;
        sendMessage = new SendMessage();
        sendMessage.setText(Messages.ALREADY_REGISTERED);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.alreadyRegistered());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }

    public static SendPhoto enterStartRegion(long id){
        SendPhoto sendPhoto = null;
        sendPhoto = new SendPhoto();

        sendPhoto.setPhoto(ImgUrl);
        sendPhoto.setCaption(Messages.ENTER_START_REGION);
        reply.setKeyboard(ReplyKeyBoard.cityZone());
        reply.setOneTimeKeyboard(true);
        reply.setResizeKeyboard(true);
        sendPhoto.setReplyMarkup(reply);
        sendPhoto.setChatId(id);
        return sendPhoto;
    }

    public static SendPhoto enterEndRegion(long id){
        SendPhoto sendPhoto = null;
        sendPhoto = new SendPhoto();

        sendPhoto.setPhoto(ImgUrl);
        sendPhoto.setCaption(Messages.ENTER_END_REGION);
        reply.setKeyboard(ReplyKeyBoard.cityZone());
        reply.setOneTimeKeyboard(true);
        reply.setResizeKeyboard(true);
        sendPhoto.setReplyMarkup(reply);
        sendPhoto.setChatId(id);
        return sendPhoto;
    }

    public static SendMessage enterStartMonth(long id){
        SendMessage sendMessage = new SendMessage();

        sendMessage.setText(Messages.ENTER_MONTH_START);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.months());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        sendMessage.setReplyMarkup(reply);


        return sendMessage;
    }

    public static SendMessage enterDayOfMonth(long id , int day){

        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.EnterDayOfMonth(day));
        sendMessage.setChatId(id);
        return sendMessage;
    }

    public static SendMessage enterHourStart(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_HOUR_START);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.hours());
        reply.setOneTimeKeyboard(true);
        reply.setResizeKeyboard(true);

        sendMessage.setReplyMarkup(reply);

        return  sendMessage;
    }

    public static SendMessage enterHourEnd(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_HOUR_END);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.hours());
        reply.setOneTimeKeyboard(true);
        reply.setResizeKeyboard(true);

        sendMessage.setReplyMarkup(reply);

        return sendMessage;
    }

    public static SendMessage endOfrequests(long id){
        SendMessage sendMessage = new SendMessage();

        sendMessage.setText(Messages.END_OF_REQUESTS);
        sendMessage.setChatId(id);

        return sendMessage;

    }

    public static SendMessage enterCorrectCommand(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_CORRECT_COMMAND);
        sendMessage.setChatId(id);

        return sendMessage;
    }

    public static SendMessage howManyDays(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.DAYS_SERVICE);
        sendMessage.setChatId(id);

        return sendMessage;
    }

    public static SendMessage enterOriginCity(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_ORIGIN_CITY);
        sendMessage.setChatId(id);
        reply.setKeyboard(ReplyKeyBoard.originCities());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);
        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }

    public static SendMessage enterDestProvince(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_DEST_PROVINCE);
        sendMessage.setChatId(id);

        reply.setKeyboard(ReplyKeyBoard.provinces());
        reply.setOneTimeKeyboard(true);
        reply.setResizeKeyboard(true);
        sendMessage.setReplyMarkup(reply);

        return sendMessage;
    }

    public static SendMessage enterDestCity(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_DEST_CITY);
        sendMessage.setChatId(id);

        return sendMessage;
    }

    public static SendMessage hasReturn(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.HAS_RETURN);
        sendMessage.setChatId(id);

        reply.setResizeKeyboard(true);
        reply.setKeyboard(ReplyKeyBoard.hasReturn());
        reply.setSelective(true);

        sendMessage.setReplyMarkup(reply);
        return sendMessage;
    }


    public static SendMessage returnMonth(long id){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(id);
        sendMessage.setText(Messages.ENTER_END_MONTH);

        reply.setSelective(true);
        reply.setKeyboard(ReplyKeyBoard.months());
        reply.setResizeKeyboard(true);
        reply.setOneTimeKeyboard(true);

        sendMessage.setReplyMarkup(reply);

        return sendMessage;
    }

    public static SendMessage returnDay(long id , int day){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(Messages.ENTER_END_DAY + "(1 ,"+ day + ")");
        sendMessage.setChatId(id);

        return sendMessage;
    }

    public static SendMessage sendToAdmin(String phoneNumber , int passengerCount , int start_time , int end_time ,
                                          int start_zone , int end_zone , int days , int id , String name , String last_name){
        SendMessage sendMessage = new SendMessage();

        int code = id + 100;

        String s = String.format( "مسافری با مشخصات زیر سرویس درون شهری در خواست داده است \n" + "\n کد مسافر = %d"+ "\n نام = %s" + "\n نام خانوادگی = %s" +"\n شماره تلفن = %s" + "\n تعداد مسافر = %d" + ""
                + "\n ساعت حرکت = %d" + "\n ساعت پایان = %d" + "\n منطقه مبدا = %d" + "\n منطقه مقصد = %d" + "\n تعداد روز = %d" ,code , name , last_name , phoneNumber , passengerCount, start_time
                , end_time , start_zone , end_zone , days);

        sendMessage.setText(s);
        sendMessage.setChatId(Messages.ADMIN_ID);

        return sendMessage;
    }

    public static SendMessage sendToAdminOut(String phoneNumber , int passengerCount , String originCity , String destProvince ,
                                             String destCity , int startMonth , int startDay , int endMonth , int endDay
                                            , int id , String name , String last_name){

        int code = id + 100;

        SendMessage sendMessage = new SendMessage();
        String s = String.format( "مسافری درخواست سرویس برون شهری داده است. \n"+"\n کد مسافر = %d" +"\n نام = %s"+ "\n نام خانوادگی = %s" +"\nشماره تلفن = %s" + "\n تعداد مسافر = %d "
                + "\n شهر مبدا = %s" + "\n استان مقصد = %s" + "\n شهر مقصد = %s" + "\n ماه حرکت = %d" + "\n روز حرکت = %d" +
                    "\n ماه برگشت = %d" + "\n روز برگشت = %d",code , name , last_name , phoneNumber , passengerCount , originCity , destProvince , destCity ,
                startMonth , startDay , endMonth , endDay);

        sendMessage.setText(s);
        long d = 96139854;
        sendMessage.setChatId(Messages.ADMIN_ID);

        return sendMessage;

    }




}
